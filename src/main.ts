import "./style.css"

import { interval, fromEvent, merge } from "rxjs"
import { map, filter, scan } from "rxjs/operators"

// vvvvvvvvvvvvvvvvvvvvv
// works only on chrome!
// ^^^^^^^^^^^^^^^^^^^^^

// utils

// array range
const range = <T>(
    lo: number,
    hi?: number | ((e: number) => T),
    step = 1,
    cb?: (e: number) => T,
): T[] => (
    hi instanceof Function && ((cb = hi), (hi = undefined)),
    hi ?? ((hi = lo), (lo = 0)),
    (hi = Math.ceil((hi - lo) / step)),
    hi > 0
        ? Array(hi)
              .fill(0)
              // casting x, ts can't? handle I-fns as opt. arg for generic lambdas
              .map((_, i) => (cb ?? ((x) => <T>(<unknown>x)))(i * step + lo))
        : []
)
// pure array insert
const insert = <T>(a: T[], idx: number, item: T) =>
    a.map((e, i) => (i === idx ? item : e))
// math clamp
const clamp = (val: number, min: number, max: number) =>
    Math.min(Math.max(min, val), max)
// array to (json) object - slow ~~but~~and functional
const toObject = <T, U>(
    a: readonly T[],
    kf: (i: number) => string,
    ff: (x: T) => boolean, // filter func
    vf: (e: T) => U,
): Readonly<Record<string, U>> => {
    const ret: Record<string, U> = {}
    for (const [k, v] of a.entries()) ff(v) && (ret[kf(k)] = vf(v))
    return ret
}
// perform calculation if not undefined
const undefOr = <T, U>(f: (_: T) => U, x?: T) =>
    x === undefined ? undefined : f(x)

// consts

const MSPT = 1000 / 60 // ms per tick
const ROWS = 14
const MAX_Y = <13>(ROWS - 1) // floored max y coord
const COLS = 14
const MAX_X = COLS - 1 // floored max x coord
const WIDTH = 600
const ROW_MAXRANGE = 42 // in tiles [0..14]
const GAME_SVG_ID = "svgCanvas"
const ROW_ID_PREFIX = "row"
const SCORE_ROW_ID = `${ROW_ID_PREFIX}0`
const COVE_ROW_ID = `${ROW_ID_PREFIX}1`
const HITBOX = 0.25 // [0..1] of hitbox ignored
const COVES = 5
const SCORE_NEWROW = 10
const GLOBAL_SPEED = 0.5
const SINK_PERIOD = 2 // secs between two sinks
const SNAKE_PERIOD = 8 // secs between complete cycle
const FROG_STARTX = Math.ceil(MAX_X / 2)
const FROG_STARTY = MAX_Y
const SINK_PERCENTAGE = 0.5 // sunk [0..1] of the time
const SINK = 2 // sinking turtle spawns every x items on valid row
const SNAKE_LENGTH = 2
const ROT_LUT: Readonly<Record<Direction, number>> = {
    north: 0,
    east: 90,
    south: 180,
    west: 270,
}
const LOG = false // impure if true

// obtain absolute x position given a cove index
const coveXPos = (i: number) => ((i + 0.29) / COVES) * COLS
// get absolute x position of a row cell on the grid
const widthToCell = (x: number) => (WIDTH / COLS) * x
// loop a given range on a smaller (hardcoded) range
// todo will take a long time to normalize if processtick not ran often enough eg. timer is throttled
const loopRowRange = (x: number) =>
    x < ROW_MAXRANGE * -0.25
        ? x + ROW_MAXRANGE
        : x > ROW_MAXRANGE * 0.75
        ? x - ROW_MAXRANGE
        : x

// events

class Tick {
    constructor(
        private readonly then?: number,
        public readonly now: number = window.performance.now(),
    ) {}
    // in secs
    get timediff() {
        return (this.now - (this.then ?? this.now)) / 1e3
    }
}
class Move {
    constructor(public readonly dir: Direction) {}
    get x(): -1 | 0 | 1 {
        return this.dir === "west" ? -1 : this.dir === "east" ? 1 : 0
    }
    get y(): -1 | 0 | 1 {
        return this.dir === "south" ? 1 : this.dir === "north" ? -1 : 0
    }
}
class Reset {
    constructor() {}
}

// state

type MovableType = "car" | "turtle" | "plank"
type Section = "road" | "water"
type ValidRowLength = 1 | 2 | 3 | 4 | 5 | 6
type Row = Readonly<{
    type: MovableType
    length: ValidRowLength
    direction: "left" | "right"
    speed: number // x-coord velocity/multiplier
    section: Section // determines various game logic
    items: ReadonlyArray<{
        // x-coords are lhs-aligned
        x: number
        // sunk/snakeX: undefined if no such capability
        sunk?: boolean
        // offset, magnitude-only, sign used to determine incr/decr when ticking
        snakeX?: number
    }>
}>
type Direction = "west" | "east" | "north" | "south"
// based off row coords - skips score and cove:
type ValidYPos = 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13
type State = Readonly<{
    difficulty: number // [1..]
    frog: Readonly<{
        // cols/x are 0-indexd, rows/y are 1-idxed
        x: number // fractional, 0-13
        y: ValidYPos
        facing: Direction
    }>
    // determines text at the top - "won" fades out after some time
    gameState: "alive" | "dead" | "won"
    score: number
    highScore: number
    // for scorekeeping; resets the moment a frog enters a cove
    elapsed: number
    // also for scorekeeping - shows when
    farthestRow: ValidYPos
    // cove state, bool[5] of if_frog_occupying
    cove: boolean[]
    rows: ReadonlyArray<Row | null>
}>

type UseAttrDict = Readonly<Record<string, string>> // <use> elems
type OutState = Readonly<{
    // { <g>: [<use> attrs] <- children }:
    uses: Readonly<Record<string, ReadonlyArray<UseAttrDict>>>
    text: Readonly<{
        score: string
        highscore: string
    }>
    flags: Readonly<{
        dead: boolean
        won: boolean
    }>
}>

// functions to generate a new copy of a clean (sub-)state on call
const createRow = (
    length: ValidRowLength,
    speed: number,
    spacing: number,
    direction: 0 | 1,
    section: Section,
    type: MovableType,
    hasSnakes?: number, // also no. of items per snake
): Row => ({
    type,
    length,
    speed,
    direction: direction === 0 ? "left" : "right",
    section,
    items: range(0, ROW_MAXRANGE - length, length + spacing, (x) => ({
        x,
        sunk: type === "turtle" && !(x % SINK) ? !1 : undefined,
        snakeX: hasSnakes != null && !(x % hasSnakes) ? 0 : undefined,
    })),
})
const initCove = (): boolean[] => range(COVES, () => false)
const initState = (): State => ({
    elapsed: 0, // in seconds
    farthestRow: MAX_Y, // farthest is min(y)
    frog: { x: FROG_STARTX, y: FROG_STARTY, facing: "north" },
    gameState: "alive",
    score: 0,
    difficulty: 1,
    highScore: 0,
    cove: initCove(),
    rows: [
        null, // score
        null, // cove
        createRow(4, 3, 4, 1, "water", "plank", 3),
        createRow(2, 2, 3, 0, "water", "turtle"),
        createRow(6, 5, 15, 1, "water", "plank", 1),
        createRow(3, 2, 4, 1, "water", "plank"),
        createRow(3, 6, 2, 0, "water", "turtle"),
        null, // safezone
        createRow(2, 3, 2, 0, "road", "car"),
        createRow(1, 12, 8, 1, "road", "car"),
        createRow(1, 5, 4, 0, "road", "car"),
        createRow(1, 4, 3, 1, "road", "car"),
        createRow(1, 5, 4, 0, "road", "car"),
        null, // safezone
    ],
})

// game

// true on collision with any movable in row
const hasCollision = ({ rows, frog }: State) => {
    const frow = rows[frog.y]
    // collides if: frog outside every item vs. frog inside some item:
    return frow?.section === "water"
        ? frow.items.every(
              ({ x, sunk, snakeX }) =>
                  sunk || // if sunk, consider the item "outside"
                  // assume frog hitbox to be smaller than 1
                  frog.x < x - HITBOX ||
                  frog.x + 1 - HITBOX > x + rows[frog.y]!.length ||
                  undefOr(
                      (sx) =>
                          // reuse the very lenient frog hitbox
                          frog.x > x + Math.abs(sx) - HITBOX &&
                          frog.x + 1 - HITBOX < x + Math.abs(sx) + SNAKE_LENGTH,
                      snakeX,
                  ),
          )
        : frow?.section === "road"
        ? frow.items.some(
              ({ x }) =>
                  frog.x + 1 - HITBOX > x &&
                  frog.x < x + rows[frog.y]!.length - HITBOX,
          )
        : false
}
// bounds checking, change facing direction, perform scoring
const processFrog = (s: State, { x: xd, y: yd, dir }: Move): State => {
    // (cast) values are clamped to range of ValidYPos:
    const y = <ValidYPos>clamp(s.frog.y + yd, 2, MAX_Y)
    const s2: State = {
        ...s,
        frog: {
            // clamp x; frog is pushed by the screen edge off a movable
            x: (s.rows[y]?.section === "water" ? <T>(x: T) => x : Math.floor)(
                clamp(s.frog.x + xd, 0, MAX_X),
            ),
            y,
            facing: dir,
        },
    }
    // perform scoring for frog movement
    const s3: State = {
        ...s2,
        // (cast) min of two ranges gives the same range:
        farthestRow: <ValidYPos>Math.min(s2.farthestRow, s2.frog.y),
        // add score when frog advances to a new row:
        score: s2.score + SCORE_NEWROW * +(s2.farthestRow > s2.frog.y),
    }
    // frog enters cove - add score, reset game
    const inRange =
        s.frog.y === 2 && dir === "north"
            ? s.cove.findIndex(
                  (_, i) =>
                      coveXPos(i) - 1 < s.frog.x &&
                      coveXPos(i) + 2 > s.frog.x + 1,
              )
            : -1 // reuse negative result of findIndex
    // perform scoring for round
    const score = s3.score + Math.max(60 - s3.elapsed, 0) * s3.difficulty
    const s4: State =
        inRange === -1
            ? s3
            : {
                  ...initState(),
                  difficulty: s3.difficulty,
                  gameState: "won",
                  score,
                  highScore: Math.max(s3.highScore, score),
                  // can enter a cove twice, will increment score but not make progress
                  cove: insert(s3.cove, inRange, true),
                  rows: s3.rows,
              }
    // increment difficulty if cove is full
    return s4.cove.every((x) => x)
        ? {
              ...s4,
              cove: initCove(),
              difficulty: s3.difficulty + 1,
          }
        : s4
}
// write elapsed to state, move movables and frog
const performTick = (s: State, { timediff }: Tick): State => {
    const elapsed = s.elapsed + timediff
    const rdiff = (r: Row) =>
        (r.direction === "left" ? -1 : 1) *
        (GLOBAL_SPEED * timediff * r.speed * 1.3 ** s.difficulty)
    const rows = s.rows.map((r) =>
        r != null
            ? {
                  ...r,
                  items: r.items.map(({ x, sunk, snakeX }) => ({
                      x: loopRowRange(x + rdiff(r)),
                      sunk: undefOr(
                          () =>
                              elapsed % SINK_PERIOD >
                              SINK_PERIOD * SINK_PERCENTAGE,
                          sunk,
                      ),
                      snakeX: undefOr(
                          () =>
                              (r.length - SNAKE_LENGTH) / 2 +
                              ((r.length - SNAKE_LENGTH) / 2) *
                                  Math.sin(
                                      (((elapsed / SNAKE_PERIOD) * 2) % 2) *
                                          Math.PI,
                                  ),
                          snakeX,
                      ),
                  })),
              }
            : r,
    )
    return {
        ...s,
        elapsed, // elapsed != now; resets on game-reset
        // expire notification after some time:
        gameState: s.gameState === "won" && elapsed > 2 ? "alive" : s.gameState,
        frog: {
            ...s.frog,
            x:
                rows[s.frog.y]?.section === "water"
                    ? clamp(s.frog.x + rdiff(rows[s.frog.y]!), 0, MAX_X)
                    : s.frog.x,
        },
        rows,
    }
}

// convert state to elements, attributes and text
const createAttributes = (s: State): OutState => ({
    uses: {
        // { row[0-9]+: AttrDict }
        ...toObject(
            s.rows,
            (i) => `${ROW_ID_PREFIX}${i}`,
            (x) => x != null,
            (e) =>
                // '!' casts, nulls filtered
                e!.items.map(({ x, sunk }) => ({
                    href: `#${sunk ? "sunk" : ""}${e!.type}${e!.length}${
                        e!.type === "car" ? e!.direction : ""
                    }`,
                    x: `calc(100% * ${x} / ${COLS})`,
                })),
        ),
        snakes: s.rows
            .flatMap((r, i) =>
                r?.type === "plank"
                    ? r.items
                          .filter((e) => e.snakeX !== null)
                          .map(
                              ({ x, snakeX }): UseAttrDict => ({
                                  href: "#snake2",
                                  x: `calc(100% * ${snakeX! + x} / ${COLS})`,
                                  y: `calc(100% * ${i} / ${ROWS})`,
                              }),
                          )
                    : [null],
            )
            // (cast) ts doesn't recognize null filter here
            .filter((e): e is UseAttrDict => e != null),
        [COVE_ROW_ID]: s.cove
            .slice(0, COVES) // extra measure
            .map((e, i) => ({
                href: `#cove${e ? "frog" : ""}`,
                x: `calc(100% * ${coveXPos(i)} / ${COLS})`,
            })),
        player: [
            {
                href: "#frog",
                x: `calc(100% * ${s.frog.x} / ${COLS})`,
                y: `calc(100% * ${s.frog.y} / ${ROWS})`,
                transform: `rotate(${ROT_LUT[s.frog.facing]} ${widthToCell(
                    s.frog.x + 0.5,
                )} ${widthToCell(s.frog.y + 0.5)})`,
            },
        ],
    },
    text: {
        score: s.score.toFixed(),
        highscore: s.highScore.toFixed(),
    },
    flags: { dead: s.gameState === "dead", won: s.gameState === "won" },
})

// observables

const keyDown = <T>(keys: string[], result: () => T) =>
    fromEvent<KeyboardEvent>(document, "keydown").pipe(
        filter(({ key }) => keys.includes(key)),
        map(result),
    )
const gameLoop = (s: State, e: Move | Tick | Reset): State => {
    LOG && e instanceof Move && console.log(s)
    LOG &&
        e instanceof Tick &&
        e.timediff > MSPT / 500 && // warn if frame time -lt 60/2=30fps
        console.log(`frame took ${(e.timediff * 1000).toFixed()}ms!`)
    const s2 =
        e instanceof Move && s.gameState !== "dead"
            ? processFrog(s, e)
            : e instanceof Tick && s.gameState !== "dead"
            ? performTick(s, e)
            : e instanceof Reset
            ? {
                  ...initState(),
                  highScore: s.highScore,
              }
            : s
    return hasCollision(s2)
        ? {
              ...s2,
              gameState: "dead",
              highScore: Math.max(s2.score, s2.highScore),
          }
        : s2
}
const main = () =>
    merge(
        // window.requestAnimationFrame
        // don't rely on interval() vals, use sys. clock
        interval(MSPT).pipe(scan((a) => new Tick(a.now), new Tick())),
        keyDown(["ArrowLeft", "a"], () => new Move("west")),
        keyDown(["ArrowRight", "d"], () => new Move("east")),
        keyDown(["ArrowUp", "w"], () => new Move("north")),
        keyDown(["ArrowDown", "s"], () => new Move("south")),
        keyDown(["r"], () => new Reset()),
    )
        .pipe(scan(gameLoop, initState()), map(createAttributes))
        .subscribe(({ uses, text, flags }: OutState) => {
            // assign to key of nullable obj
            const assign = <T, U extends keyof T>(o: T | null, k: U, v: T[U]) =>
                o != null && (o[k] = v)
            // get typed <g> element
            const getG = (id: string) =>
                <SVGGElement & HTMLElement>document.getElementById(id)
            const svg = document.getElementById(GAME_SVG_ID)!

            // create or update all <use> elements in a given <g> elem in `use` by id
            Object.entries(uses).forEach(([gId, use]) => {
                const g = getG(gId)
                // gc children that won't have their attributes replaced here
                ;[].forEach.bind(g.children)(
                    (e: SVGUseElement, i) => i > use.length && g.removeChild(e),
                )
                // populate attributes according to given lookup table `e`
                use.forEach((e, i) => {
                    // create or reuse <use>
                    // assuming attributes of all elems in a <g> always have the same structure,
                    // such that there is no need to worry about orphaned attributes
                    const id = `${gId}-${i}`
                    const el =
                        document.getElementById(id) ??
                        g.appendChild(
                            document.createElementNS(svg.namespaceURI, "use"),
                        )
                    Object.entries(e).forEach(([k, v]) => el.setAttribute(k, v))
                    el.setAttribute("id", id)
                })
            })
            // change game text
            Object.entries(text).forEach(([k, v]) =>
                assign(document.getElementById(k), "textContent", v),
            )
            // modify flags that trigger styles
            Object.entries(flags).forEach(([k, v]) =>
                getG(SCORE_ROW_ID).classList[v ? "add" : "remove"](k),
            )
        })
document.readyState === "loading"
    ? window.addEventListener("DOMContentLoaded", main)
    : main()
